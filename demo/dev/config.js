export default{
    navDataUrl: 'dev/data/menu.json',
    tableDataUrl: 'dev/data/table.json',
    docFrameUrl: '../doc/frame.md',
    
    // navDataUrl: 'http://gavinzhulei.vicp.cc/advanceback/advanceback/demo/dev/data/menu.json',
    // tableDataUrl: 'http://gavinzhulei.vicp.cc/advanceback/advanceback/demo/dev/data/table.json'
}