import Vue from 'vue'
import Router from 'vue-router'
import Frame from './frame.vue'


import RouteConfig from './route.config'

require('../../src/style/comm.scss')
require('../../src/style/toast.scss')

Vue.use(Router)

const router = window.router = new Router();

router.map(RouteConfig)

router.redirect({
'*': '/home'
})

Vue.config.debug = true;

router.start(Frame, '#app-main');