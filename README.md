##advanceback开发说明

项目是基于metronic（目录位于assets/下）样式开发的一套SPA（单页面应用）后台管理框架

阅读代码需了解一个MVVM的库-- Vue.js，请参考文档：[Vue.js](http://cn.vuejs.org/guide/)


[demo](http://api.tingche.in/common/ui/demo/index.html)

让我们开始快速开发吧：

1. `git clone https://git.oschina.net/gavinzhulei/advanceback.git` 克隆项目
2. `cd advanceback` 进入项目根目录
3. `npm install` 安装项目所依赖的包
4. `webpack --watch` 运行实时编译环境
5. 开始进入代码旅途，本项目提供了一个例子（demo/），可进行参考